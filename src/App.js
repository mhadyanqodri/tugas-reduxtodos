import React from "react";
import { 
  SafeAreaView,
  ScrollView,
  useColorScheme,
} from 'react-native';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { Provider } from 'react-redux';
import { Store } from './Store';

import Todos from "./Containers/Todos";

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <Provider store={Store}>
      <SafeAreaView style={backgroundStyle}>
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={backgroundStyle}>
          <Todos />
        </ScrollView>
      </SafeAreaView>
    </Provider>
  )
}

export default App;