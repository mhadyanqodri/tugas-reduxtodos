import axios from 'axios';
import {Alert} from 'react-native'
import { useDispatch } from 'react-redux';
import {
    FETCH_TODO_SUCCESS,
    CREATE_TODO_SUCCESS,
    UPDATE_TODO_SUCCESS,
    DELETE_TODO_SUCCESS
} from '../Types';

export const saveTodos = (data) => ({
    type: FETCH_TODO_SUCCESS,
    payload: data
});

export const inputTodos = (data) => ({
    type: CREATE_TODO_SUCCESS,
    payload: data
});

export const updateTodos = () => ({
    type: UPDATE_TODO_SUCCESS,
    payload: data,
});

export const eraseTodo = () => ({
    type: DELETE_TODO_SUCCESS,
    payload: id,
})

export const editTodos = (id, data) => async dispatch => {
    try{
        await axios.patch(`http://code.aldipee.com/api/v1/todos/${id}`, data).then(() => {
            Alert.alert('Berhasil', 'Data berhasil diupdate');
        });
        dispatch(updateTodos(data))
    } catch (err) {
        console.log(err);
    }
}

export const deleteTodos = (id) => async dispatch => {
    try {
        await axios.delete(`http://code.aldipee.com/api/v1/todos/${id}`);
        dispatch(eraseTodo(id));
    } catch (err) {
        console.log(err);
    }
}

export const createTodos = (data) => async dispatch => {
    try {
        await axios.post(`http://code.aldipee.com/api/v1/todos`, data).then(() => {
          Alert.alert('sukses', 'Data berhasil ditambahkan');
        });
        dispatch(inputTodos(data));
      } catch (err) {
        console.log(err);
    }
    console.log(data)
}

export const getTodos = () => {
    return async (dispatch) => {
        const resTodos = await axios.get('http://code.aldipee.com/api/v1/todos');
        if (resTodos.data.results.length > 0) {
            dispatch(saveTodos(resTodos.data.results));
        }
    };
};
