import { CREATE_TODO_SUCCESS, FETCH_TODO_SUCCESS, UPDATE_TODO_SUCCESS, DELETE_TODO_SUCCESS } from "../Types";
const initialState = {
    todos: [ ]
};

const Reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TODO_SUCCESS:
            return {
                ...state,
                todos: action.payload,
            };

        case CREATE_TODO_SUCCESS:
            return {
                ...state,
                todos: [...state.todos, action.payload]
            };

        case UPDATE_TODO_SUCCESS:
            return {
                ...state,
                todos: [...state.todos, action.payload]
            };

        case DELETE_TODO_SUCCESS:
            return {
                ...state,
                todos: state.todos.filter(el => el.id !== action.payload),
            }
        default:
            return state;
    }
};
export default Reducer;