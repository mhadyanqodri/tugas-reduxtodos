import React, {useEffect, useState} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    TextInput,
    ActivityIndicator,
    ScrollView,
    Alert
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import styles from './styles';
import { createTodos, editTodos, getTodos, deleteTodos } from '../../actions';
import {useDispatch, useSelector} from 'react-redux';


const Todos = () => {

    const todo = useSelector(data => data.todos)
    const dispatch = useDispatch()
    // const loading = useSelector(data => data.loading)

    const [isLoading, setIsLoading] = useState(false)
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [selectedStatus, setSelectedStatus] = useState('ON_PROGRESS');
    const [button, setButton] = useState('Save');
    const [selectedUser, setSelectedUser] = useState({});

    const onSubmit = () => {
        const dataSent = {
            title: title,
            description: description,
            status: selectedStatus,
        };

        if (button === 'Save'){
            dispatch(createTodos(dataSent));
            setTitle('');
            setDescription('');
            dispatch(getTodos());
        } else if (button === 'Update') {
            dispatch(editTodos(selectedUser.id, dataSent));
            setTitle('');
            setDescription('');
            dispatch(getTodos());
            setButton('Save');
        }
    };

    const onCancel = () => {
        setDescription('');
        setTitle('');
        setButton('Save');
    };

    const selectedItem = (item) => {
        setSelectedUser(item);
        setTitle(item.title);
        setDescription(item.description);
        setButton('Update')
    }

    

    useEffect(() => {
        setIsLoading(true)
        dispatch(getTodos()) 
        setIsLoading(false)
    //   console.log(todo)
    }, [])
    

    return (
        <>
            {isLoading ? <ActivityIndicator /> :
                <View style={styles.root}>
                    <View style={styles.headingContainer}>
                        <Text style={styles.heading}>Redux Todo List</Text>
                    </View>

                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.input}
                            onChangeText={text => setTitle(text)}
                            value={title}
                            placeholder="Title"
                            placeholderTextColor="white"

                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => setDescription(text)}
                            value={description}
                            placeholder="Description"
                            placeholderTextColor="white"

                        />
                    </View>
                    <View style={styles.statusesContainer}>
                        <TouchableOpacity
                            onPress={() => setSelectedStatus('ON_PROGRESS')}
                            style={[
                                styles.statusButton,
                                selectedStatus === 'ON_PROGRESS' && styles.statusButtonSelected
                            ]}
                        >
                            <Text
                                style={[
                                    styles.statusButtonText,
                                    selectedStatus === 'ON_PROGRESS' && styles.statusButtonTextSelected
                                ]}
                            >
                                Progress
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => setSelectedStatus('DONE')}
                            style={[
                                styles.statusButton,
                                selectedStatus === 'DONE' && styles.statusButtonSelected
                            ]}
                        >
                            <Text
                                style={[
                                    styles.statusButtonText,
                                    selectedStatus === 'DONE' && styles.statusButtonTextSelected
                                ]}
                            >
                                Done
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: 'auto', marginTop: 20 }}>
                        <TouchableOpacity style={styles.saveButon} onPress={() => onSubmit()}>
                            <Text style={styles.buttonText}>{button}</Text>
                            <Icon name="save" size={20} color="white" />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cancelButton} onPress={() => onCancel()}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.content}>
                        <Text style={styles.todoDateText}>Monday</Text>
                        <ScrollView>
                            {todo.map(itemTodo => (
                                <View key={itemTodo.id} style={styles.cardListContainer}>
                                    <View style={styles.todoCard}>
                                        <View style={styles.todoTitleContainer}>
                                            <View style={styles.todoActionContainer}>
                                                <Text style={styles.todoTitle}>{itemTodo.title}</Text>
                                                <TouchableOpacity style={styles.doneBadge}>
                                                    <Text style={styles.doneBadgeText}>{itemTodo.status}</Text>
                                                </TouchableOpacity>
                                            </View>

                                            <View style={styles.todoActionContainer}>
                                                <TouchableOpacity style={styles.editButton} onPress={() => selectedItem(itemTodo)}>
                                                    <Icon name="edit" size={20} color="white" />
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={() => Alert.alert(
                                                    'Peringatan', 'Apakah anda yakin ingin menghapus?',
                                                    [
                                                        {
                                                            text: 'tidak'
                                                        },
                                                        {
                                                            text: 'Ya',
                                                            onPress: () => dispatch(deleteTodos(itemTodo.id)),
                                                        },
                                                    ],
                                                )}>
                                                    <Icon name="trash-2" size={20} color="white" />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View>
                                            <Text style={styles.todoDescription}>{itemTodo.title}</Text>
                                        </View>
                                    </View>
                                </View>
                            ))}
                        </ScrollView>
                    </View>
                </View>
            }
        </>
    )
}


export default Todos;