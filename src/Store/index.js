import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import ReduxThunk from 'redux-thunk';
import thunk from "redux-thunk";
import TodosReducer from '../Reducers';


// const Reducers = {
//     appData: TodosReducer
// }

const middlewares = [ReduxThunk];

export const Store = createStore(TodosReducer, compose(applyMiddleware(...middlewares)))

// export const Store = createStore(combineReducers(Reducers), applyMiddleware(ReduxThunk))